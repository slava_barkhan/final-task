package by.barkhan.finaltask.factory;

import javax.servlet.http.HttpServletRequest;

import by.barkhan.finaltask.command.ActionCommand;
import by.barkhan.finaltask.command.EmptyCommand;
import by.barkhan.finaltask.constant.FormParameter;
import by.barkhan.finaltask.enums.CommandType;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ActionFactory {
	private static final Logger LOG = LogManager.getLogger(ActionFactory.class);

	public ActionCommand defineCommand(HttpServletRequest request) {
		ActionCommand current = new EmptyCommand();
		String action = request.getParameter(FormParameter.COMMAND);
		if (action == null || action.isEmpty()) {
			return current;
		}
		try {
			CommandType currentEnum = CommandType.valueOf(action.toUpperCase());
			current = currentEnum.getCurrentCommand();
		} catch (IllegalArgumentException e) {
			LOG.log(Level.ERROR,e);
		}
		return current;
	}
}
