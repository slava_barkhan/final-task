package by.barkhan.finaltask.entity;

public class Period extends Entity {
	private static final long serialVersionUID = -7318413421736537304L;

	private String periodicity;

	public Period() {
	}

	public Period(String periodicity) {
		this.periodicity = periodicity;
	}

	public Period(int id, String periodicity) {
		super(id);
		this.periodicity = periodicity;
	}

	public String getPeriodicity() {
		return periodicity;
	}

	public void setPeriodicity(String periodicity) {
		this.periodicity = periodicity;
	}

	public String getReport() {
		return getPeriodicity();
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Period)) return false;

		Period period = (Period) o;

		return periodicity != null ? periodicity.equals(period.periodicity) : period.periodicity == null;
	}

	@Override
	public int hashCode() {
		return periodicity != null ? periodicity.hashCode() : 0;
	}

	@Override
	public String toString() {
		return "Period [periodicity=" + periodicity + "]";
	}

}
