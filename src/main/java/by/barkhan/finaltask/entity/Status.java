package by.barkhan.finaltask.entity;

public class Status extends Entity {
	private static final long serialVersionUID = 2347517611650124975L;

	private String description;

	public Status() {
	}

	public Status(String description) {
		this.description = description;
	}

	public Status(int id, String description) {
		super(id);
		this.description = description;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Status)) return false;

		Status status = (Status) o;

		return description != null ? description.equals(status.description) : status.description == null;
	}

	@Override
	public int hashCode() {
		return description != null ? description.hashCode() : 0;
	}

	@Override
	public String toString() {
		return "Status [description=" + description + "]";
	}

}
