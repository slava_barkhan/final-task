package by.barkhan.finaltask.entity;

public class Reservation extends Entity {
	private static final long serialVersionUID = 8409441552942455L;

	private User user;
	private Subscription subscription;
	private Status status;
	private int count;

	public Reservation() {
	}

	public Reservation(int id, int count) {
		super(id);
		this.count = count;
	}

	public Reservation(int count) {
		this.count = count;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Subscription getSubscription() {
		return subscription;
	}

	public void setSubscription(Subscription subscription) {
		this.subscription = subscription;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	@Override
	public String toString() {
		return "Reservation [user=" + user + ", subscription=" + subscription
				+ ", status=" + status.getDescription() + ", count=" + count
				+ "]";
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Reservation)) return false;

		Reservation that = (Reservation) o;

		if (count != that.count) return false;
		if (user != null ? !user.equals(that.user) : that.user != null) return false;
		if (subscription != null ? !subscription.equals(that.subscription) : that.subscription != null) return false;
		return status != null ? status.equals(that.status) : that.status == null;
	}

	@Override
	public int hashCode() {
		int result = user != null ? user.hashCode() : 0;
		result = 31 * result + (subscription != null ? subscription.hashCode() : 0);
		result = 31 * result + (status != null ? status.hashCode() : 0);
		result = 31 * result + count;
		return result;
	}
}
