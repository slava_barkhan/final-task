package by.barkhan.finaltask.entity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Magazine extends Entity {
	private static final long serialVersionUID = -2824763500938665004L;

	private String title;
	private String annotation;
	private Period period;
	private String location;
	private ArrayList<Subscription> subs = new ArrayList<Subscription>();

	public Magazine() {
	}

	public Magazine(String title, String annotation) {
		this.title = title;
		this.annotation = annotation;
	}

	public Magazine(int id, String title, String annotation) {
		super(id);
		this.title = title;
		this.annotation = annotation;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAnnotation() {
		return annotation;
	}

	public void setAnnotation(String annotation) {
		this.annotation = annotation;
	}

	public Period getPeriod() {
		return period;
	}

	public void setPeriod(Period period) {
		this.period = period;
	}

	public String getReport() {
		return getTitle();
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public void addSubscription(Subscription sub) {
		this.subs.add(sub);
	}

	public void addSubscriptions(List<Subscription> subList) {
		this.subs.addAll(subList);
	}

	public List<Subscription> getSubs() {
		return Collections.unmodifiableList(subs);
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Magazine)) return false;

		Magazine magazine = (Magazine) o;

		if (title != null ? !title.equals(magazine.title) : magazine.title != null) return false;
		if (annotation != null ? !annotation.equals(magazine.annotation) : magazine.annotation != null) return false;
		if (period != null ? !period.equals(magazine.period) : magazine.period != null) return false;
		if (location != null ? !location.equals(magazine.location) : magazine.location != null) return false;
		return subs != null ? subs.equals(magazine.subs) : magazine.subs == null;
	}

	@Override
	public int hashCode() {
		int result = title != null ? title.hashCode() : 0;
		result = 31 * result + (annotation != null ? annotation.hashCode() : 0);
		result = 31 * result + (period != null ? period.hashCode() : 0);
		result = 31 * result + (location != null ? location.hashCode() : 0);
		result = 31 * result + (subs != null ? subs.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		return "Magazine [id=" + getId() + "title=" + title + ", annotation="
				+ annotation + ", period=" + period.getPeriodicity()
				+ ", location=" + location + "]";
	}

}
