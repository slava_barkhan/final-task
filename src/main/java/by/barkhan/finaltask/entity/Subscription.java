package by.barkhan.finaltask.entity;

public class Subscription extends Entity {
	private static final long serialVersionUID = -1046895540428914609L;

	private Magazine magazine;
	private String index;
	private String duration;
	private String price;

	public Subscription() {
	}

	public Subscription(String index, String duration, String price) {
		super();
		this.index = index;
		this.duration = duration;
		this.price = price;
	}

	public Subscription(int id, String index, String duration, String price) {
		super(id);
		this.index = index;
		this.duration = duration;
		this.price = price;
	}

	public Magazine getMagazine() {
		return magazine;
	}

	public void setMagazine(Magazine magazine) {
		this.magazine = magazine;
	}

	public String getIndex() {
		return index;
	}

	public void setIndex(String index) {
		this.index = index;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Subscription)) return false;

		Subscription that = (Subscription) o;

		if (magazine != null ? !magazine.equals(that.magazine) : that.magazine != null) return false;
		if (index != null ? !index.equals(that.index) : that.index != null) return false;
		if (duration != null ? !duration.equals(that.duration) : that.duration != null) return false;
		return price != null ? price.equals(that.price) : that.price == null;
	}

	@Override
	public int hashCode() {
		int result = magazine != null ? magazine.hashCode() : 0;
		result = 31 * result + (index != null ? index.hashCode() : 0);
		result = 31 * result + (duration != null ? duration.hashCode() : 0);
		result = 31 * result + (price != null ? price.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		return "Subscription [id=" + getId() + "magazine=" + magazine
				+ ", index=" + index + ", duration=" + duration + ", price="
				+ price + "]";
	}
}
