package by.barkhan.finaltask.entity;

public class User extends Entity {
	private static final long serialVersionUID = 8616481033020433776L;

	private String userName;
	private int age;
	private String login;
	private String password;
	private String roleName = "reader";

	public User() {
	}

	public User(String userName, int age, String login) {
		this.userName = userName;
		this.age = age;
		this.login = login;
	}

	public User(int id, String userName, int age, String login) {
		super(id);
		this.userName = userName;
		this.age = age;
		this.login = login;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof User)) return false;

		User user = (User) o;

		if (age != user.age) return false;
		if (userName != null ? !userName.equals(user.userName) : user.userName != null) return false;
		if (login != null ? !login.equals(user.login) : user.login != null) return false;
		if (password != null ? !password.equals(user.password) : user.password != null) return false;
		return roleName != null ? roleName.equals(user.roleName) : user.roleName == null;
	}

	@Override
	public int hashCode() {
		int result = userName != null ? userName.hashCode() : 0;
		result = 31 * result + age;
		result = 31 * result + (login != null ? login.hashCode() : 0);
		result = 31 * result + (password != null ? password.hashCode() : 0);
		result = 31 * result + (roleName != null ? roleName.hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		return "User [id=" + getId() + ", userName=" + userName + ", age="
				+ age + ", role=" + roleName + "]";
	}

}
