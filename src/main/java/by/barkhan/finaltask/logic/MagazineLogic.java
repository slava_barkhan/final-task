package by.barkhan.finaltask.logic;

import java.util.List;

import by.barkhan.finaltask.dao.MagazineDAO;
import by.barkhan.finaltask.dao.SubscriptionDAO;
import by.barkhan.finaltask.dbhelper.ConnectionPool;
import by.barkhan.finaltask.dbhelper.ProxyConnection;
import by.barkhan.finaltask.entity.Period;
import by.barkhan.finaltask.entity.Subscription;
import by.barkhan.finaltask.exception.DAOException;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.barkhan.finaltask.dao.PeriodDAO;
import by.barkhan.finaltask.entity.Magazine;

public class MagazineLogic {
	private static final Logger LOG = LogManager.getLogger(MagazineLogic.class);

	public static List<Magazine> findAllMagazines(int offset) {
		ProxyConnection con = null;
		List<Magazine> magList = null;
		try {
			con = ConnectionPool.getInstance().getConnection();
			MagazineDAO magazineDao = new MagazineDAO(con);
			magList = magazineDao.findAll(offset);
		} catch (DAOException e) {
			LOG.log(Level.ERROR,e);
		} finally {
			ConnectionPool.getInstance().closeConnection(con);
		}
		return magList;
	}

	public static List<Magazine> findAllMagazines() {
		ProxyConnection con = null;
		List<Magazine> magList = null;
		try {
			con = ConnectionPool.getInstance().getConnection();
			MagazineDAO magazineDao = new MagazineDAO(con);
			magList = magazineDao.findAll();
		} catch (DAOException e) {
			LOG.log(Level.ERROR,e);
		} finally {
			ConnectionPool.getInstance().closeConnection(con);
		}
		return magList;
	}

	public static Magazine findMagazineById(int id) {
		ProxyConnection con = null;
		Magazine mag = null;
		try {
			con = ConnectionPool.getInstance().getConnection();
			MagazineDAO magazineDao = new MagazineDAO(con);
			mag = magazineDao.findEntityById(id);
			SubscriptionDAO subDao = new SubscriptionDAO(con);
			List<Subscription> subList = subDao.findByMagazineId(id);
			mag.addSubscriptions(subList);
		} catch (DAOException e) {
			LOG.log(Level.ERROR,e);
		} finally {
			ConnectionPool.getInstance().closeConnection(con);
		}
		return mag;
	}

	public static int calculateCount() {
		ProxyConnection con = null;
		int count = 0;
		try {
			con = ConnectionPool.getInstance().getConnection();
			MagazineDAO magazineDao = new MagazineDAO(con);
			count = magazineDao.calculateCount();
		} catch (DAOException e) {
			LOG.log(Level.ERROR,e);
		} finally {
			ConnectionPool.getInstance().closeConnection(con);
		}
		return count;
	}

	public static void createMagazine(int periodId, Magazine mag) {
		ProxyConnection con = null;
		try {
			con = ConnectionPool.getInstance().getConnection();
			PeriodDAO periodDao = new PeriodDAO(con);
			Period period = periodDao.findEntityById(periodId);
			mag.setPeriod(period);
			MagazineDAO magazineDao = new MagazineDAO(con);
			magazineDao.create(mag);
		} catch (DAOException e) {
			LOG.log(Level.ERROR,e);
		} finally {
			ConnectionPool.getInstance().closeConnection(con);
		}
	}

	public static void updateMagazine(int periodId, Magazine mag) {
		ProxyConnection con = null;
		try {
			con = ConnectionPool.getInstance().getConnection();
			PeriodDAO periodDao = new PeriodDAO(con);
			Period period = periodDao.findEntityById(periodId);
			mag.setPeriod(period);
			MagazineDAO magazineDao = new MagazineDAO(con);
			magazineDao.update(mag);
		} catch (DAOException e) {
			LOG.log(Level.ERROR,e);
		} finally {
			ConnectionPool.getInstance().closeConnection(con);
		}
	}

	public static void deleteMagazine(int id) {
		ProxyConnection con = null;
		try {
			con = ConnectionPool.getInstance().getConnection();
			MagazineDAO magazineDao = new MagazineDAO(con);
			magazineDao.delete(id);
		} catch (DAOException e) {
			LOG.log(Level.ERROR,e);
		} finally {
			ConnectionPool.getInstance().closeConnection(con);
		}
	}

	public static List<Period> findAllPeriods() {
		ProxyConnection con = null;
		List<Period> periodList = null;
		try {
			con = ConnectionPool.getInstance().getConnection();
			PeriodDAO periodDao = new PeriodDAO(con);
			periodList = periodDao.findAll();
		} catch (DAOException e) {
			LOG.log(Level.ERROR,e);
		} finally {
			ConnectionPool.getInstance().closeConnection(con);
		}
		return periodList;
	}
}
