package by.barkhan.finaltask.logic;

import by.barkhan.finaltask.dao.UserDAO;
import by.barkhan.finaltask.dbhelper.ProxyConnection;
import by.barkhan.finaltask.entity.User;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.barkhan.finaltask.dbhelper.ConnectionPool;
import by.barkhan.finaltask.exception.DAOException;
import by.barkhan.finaltask.exception.LogicException;

public class UserLogic {
	private static final Logger LOG = LogManager.getLogger(UserLogic.class);

	public static User checkLoginPassword(String enterLogin, String enterPass) {
		ProxyConnection con = null;
		User user = null;
		try {
			con = ConnectionPool.getInstance().getConnection();
			UserDAO userDao = new UserDAO(con);
			user = userDao.findUserByLoginPassword(enterLogin, enterPass);
		} catch (DAOException e) {
			LOG.log(Level.ERROR,e);
		} finally {
			ConnectionPool.getInstance().closeConnection(con);
		}
		return user;
	}

	public static User checkLogin(String enterLogin) {
		ProxyConnection con = null;
		User user = null;
		try {
			con = ConnectionPool.getInstance().getConnection();
			UserDAO userDao = new UserDAO(con);
			user = userDao.findUserByLogin(enterLogin);
		} catch (DAOException e) {
			LOG.log(Level.ERROR,e);
		} finally {
			ConnectionPool.getInstance().closeConnection(con);
		}
		return user;
	}

	public static User createUser(User user) {
		ProxyConnection con = null;
		try {
			con = ConnectionPool.getInstance().getConnection();
			UserDAO userdDao = new UserDAO(con);
			int userId = userdDao.create(user);
			user.setId(userId);
		} catch (DAOException e) {
			LOG.log(Level.ERROR,e);
		} finally {
			ConnectionPool.getInstance().closeConnection(con);
		}
		return user;
	}

	public static void deleteUser(int id) throws LogicException {
		ProxyConnection con = null;
		try {
			con = ConnectionPool.getInstance().getConnection();
			UserDAO userdDao = new UserDAO(con);
			userdDao.delete(id);
		} catch (DAOException e) {
			LOG.log(Level.ERROR,e);
		} finally {
			ConnectionPool.getInstance().closeConnection(con);
		}
	}
}
