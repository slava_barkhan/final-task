package by.barkhan.finaltask.logic;

import java.util.List;

import by.barkhan.finaltask.dao.UserDAO;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.barkhan.finaltask.dao.ReservationDAO;
import by.barkhan.finaltask.dao.StatusDAO;
import by.barkhan.finaltask.dao.SubscriptionDAO;
import by.barkhan.finaltask.dbhelper.ConnectionPool;
import by.barkhan.finaltask.dbhelper.ProxyConnection;
import by.barkhan.finaltask.entity.Reservation;
import by.barkhan.finaltask.entity.Status;
import by.barkhan.finaltask.entity.Subscription;
import by.barkhan.finaltask.entity.User;
import by.barkhan.finaltask.exception.DAOException;

public class ReservationLogic {
	private static final Logger LOG = LogManager.getLogger(SubscriptionLogic.class);

	public static List<Reservation> findAllreservations() {
		List<Reservation> resList = null;
		ProxyConnection con = null;
		try {
			con = ConnectionPool.getInstance().getConnection();
			ReservationDAO resDao = new ReservationDAO(con);
			resList = resDao.findAll();
		} catch (DAOException e) {
			LOG.log(Level.ERROR,e);
		} finally {
			ConnectionPool.getInstance().closeConnection(con);
		}
		return resList;
	}

	public static void createReservation(int userId, int subId, int count) {
		ProxyConnection con = null;
		try {
			con = ConnectionPool.getInstance().getConnection();
			UserDAO userDao = new UserDAO(con);
			User user = userDao.findEntityById(userId);
			SubscriptionDAO subDao = new SubscriptionDAO(con);
			Subscription sub = subDao.findEntityById(subId);
			ReservationDAO resDao = new ReservationDAO(con);
			Reservation res = new Reservation(count);
			res.setUser(user);
			res.setSubscription(sub);
			int id = resDao.create(res);
			res.setId(id);
		} catch (DAOException e) {
			LOG.log(Level.ERROR,e);
		} finally {
			ConnectionPool.getInstance().closeConnection(con);
		}
	}

	public static void changeStatus(int resId, int statusId) {
		ProxyConnection con = null;
		try {
			con = ConnectionPool.getInstance().getConnection();
			StatusDAO statusDAO = new StatusDAO(con);
			Status status = statusDAO.findEntityById(statusId);
			ReservationDAO resDao = new ReservationDAO(con);
			Reservation reserv = resDao.findEntityById(resId);
			reserv.setStatus(status);
			resDao.update(reserv);
		} catch (DAOException e) {
			LOG.log(Level.ERROR,e);
		} finally {
			ConnectionPool.getInstance().closeConnection(con);
		}
	}

	public static void deleteReservation(int resId) {
		ProxyConnection con = null;
		try {
			con = ConnectionPool.getInstance().getConnection();
			ReservationDAO resDao = new ReservationDAO(con);
			resDao.delete(resId);
		} catch (DAOException e) {
			LOG.log(Level.ERROR,e);
		} finally {
			ConnectionPool.getInstance().closeConnection(con);
		}
	}

	public static List<Reservation> findByUserId(int id) {
		List<Reservation> resList = null;
		ProxyConnection con = null;
		try {
			con = ConnectionPool.getInstance().getConnection();
			ReservationDAO resDao = new ReservationDAO(con);
			resList = resDao.findByUserId(id);
		} catch (DAOException e) {
			LOG.log(Level.ERROR,e);
		} finally {
			ConnectionPool.getInstance().closeConnection(con);
		}
		return resList;
	}

}
