package by.barkhan.finaltask.logic;

import by.barkhan.finaltask.dbhelper.ProxyConnection;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.barkhan.finaltask.dao.SubscriptionDAO;
import by.barkhan.finaltask.dbhelper.ConnectionPool;
import by.barkhan.finaltask.entity.Subscription;
import by.barkhan.finaltask.exception.DAOException;

public class SubscriptionLogic {

	private static final Logger LOG = LogManager.getLogger(SubscriptionLogic.class);

	public static Subscription findSubscriptionById(int subId) {
		Subscription sub = null;
		ProxyConnection con = null;
		try {
			con = ConnectionPool.getInstance().getConnection();
			SubscriptionDAO subscriptionDao = new SubscriptionDAO(con);
			sub = subscriptionDao.findEntityById(subId);
		} catch (DAOException e) {
			LOG.log(Level.ERROR,e);
		} finally {
			ConnectionPool.getInstance().closeConnection(con);
		}
		return sub;
	}

	public static void createSubscription(Subscription sub) {
		ProxyConnection con = null;
		try {
			con = ConnectionPool.getInstance().getConnection();
			SubscriptionDAO subscriptionDao = new SubscriptionDAO(con);
			int id = subscriptionDao.create(sub);
			sub.setId(id);
		} catch (DAOException e) {
			LOG.log(Level.ERROR,e);
		} finally {
			ConnectionPool.getInstance().closeConnection(con);
		}
	}

	public static void updateSubscription(Subscription entity) {
		ProxyConnection con = null;
		try {
			con = ConnectionPool.getInstance().getConnection();
			SubscriptionDAO subscriptionDao = new SubscriptionDAO(con);
			subscriptionDao.update(entity);
		} catch (DAOException e) {
			LOG.log(Level.ERROR,e);
		} finally {
			ConnectionPool.getInstance().closeConnection(con);
		}
	}

	public static void deleteSubscription(int subId) {
		ProxyConnection con = null;
		try {
			con = ConnectionPool.getInstance().getConnection();
			SubscriptionDAO subscriptionDao = new SubscriptionDAO(con);
			subscriptionDao.delete(subId);
		} catch (DAOException e) {
			LOG.log(Level.ERROR,e);
		} finally {
			ConnectionPool.getInstance().closeConnection(con);
		}
	}
}
