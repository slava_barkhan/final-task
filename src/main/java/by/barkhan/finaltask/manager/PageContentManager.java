package by.barkhan.finaltask.manager;

import java.util.Locale;
import java.util.ResourceBundle;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class PageContentManager {
	private ResourceBundle resourceBundle;
	private final String FILE_NAME = "pagecontent";
	private final Logger LOG = LogManager.getLogger(PageContentManager.class);

	public PageContentManager() {
		resourceBundle = ResourceBundle.getBundle(FILE_NAME,
				Locale.getDefault());
	}

	public void changeResource(Locale locale) {
		resourceBundle = ResourceBundle.getBundle(FILE_NAME, locale);
	}

	public String getProperty(String key) {
		LOG.log(Level.DEBUG,"Get property: " + resourceBundle.getLocale());
		return resourceBundle.getString(key);
	}

}
