package by.barkhan.finaltask.utility;

import by.barkhan.finaltask.logic.MagazineLogic;

public class PageCalculator {
	public static final int DEFAULT_PERPAGE = 9;

	public static int findPageCount() {
		int recordCount = MagazineLogic.calculateCount();
		int pageCount = (recordCount - 1) / DEFAULT_PERPAGE + 1;
		return pageCount;
	}
}
