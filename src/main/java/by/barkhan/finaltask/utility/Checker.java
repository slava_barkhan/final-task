package by.barkhan.finaltask.utility;


public class Checker {

	public static boolean isInteger(String str) {
		boolean flag = true;
		try {
			Integer.parseInt(str);
		} catch (NumberFormatException e) {
			flag = false;
		}
		return flag;
	}

	public static boolean inRange(int digitCount, int number) {
		if (digitCount <= 0 || digitCount >= 10) {
			return false;
		}
		double lowNumber = Math.pow(10, digitCount - 1);
		double upperNumber = Math.pow(10, digitCount);
		boolean flag = number >= lowNumber && number < upperNumber;
		return flag;
	}

}
