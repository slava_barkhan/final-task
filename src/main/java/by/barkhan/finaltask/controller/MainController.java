package by.barkhan.finaltask.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import by.barkhan.finaltask.command.ActionCommand;
import by.barkhan.finaltask.command.ActionHelper;
import by.barkhan.finaltask.constant.FormParameter;
import by.barkhan.finaltask.command.marker.*;
import by.barkhan.finaltask.enums.CommandType;
import by.barkhan.finaltask.exception.CommandException;
import by.barkhan.finaltask.factory.ActionFactory;

/**
 * <p>
 * This class represents the main and only controller of the application.
 * <p>
 * For creating and determining command type we are using <b>Factory Method</b>
 * design pattern
 *
 * @author Viachaslau Barkhan
 * @since 16 january 2018
 * @version 1.0
 *
 * @see javax.servlet.http.HttpServlet
 */

@WebServlet("/controller")
public class MainController extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String PART_PATH_REDIRECT = "controller?".concat(FormParameter.COMMAND).concat("=");

	protected void doGet(HttpServletRequest request,
						 HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	protected void doPost(HttpServletRequest request,
						  HttpServletResponse response) throws ServletException, IOException {
		processRequest(request, response);
	}

	/**
	 * <p>
	 * method which processes both get and post requests
	 *
	 * @throws CommandException
	 *             - when corresponding action returns null page
	 */
	private void processRequest(HttpServletRequest request,
								HttpServletResponse response) throws ServletException, IOException {
		ActionFactory client = new ActionFactory();
		ActionCommand command = client.defineCommand(request);
		String page = command.execute(request);
		if (page != null) {
			RequestDispatcher dispatcher = getServletContext()
					.getRequestDispatcher(page);

			StringBuilder path = new StringBuilder(PART_PATH_REDIRECT);

			if(command instanceof UserRequestControllerMarker) {
				String loginError = (String) request.getAttribute("loginError");
				if(loginError != null) {
					dispatcher.forward(request, response);
				} else {
					response.sendRedirect(path.toString());
				}
			} else if(command instanceof RequestControllerMarker) {
				if (command instanceof ReservationRequestControllerMarker) {
					HttpSession session = request.getSession();
					String role = (String) session.getAttribute(FormParameter.ROLE);
					if("moderator".equals(role)){
						path.append(CommandType.SHOW_RES_LIST);
					}else{
						path.append(CommandType.SHOW_USER_ITEM);
					}
				} else if (command instanceof MagazineRequestControllerMarker) {
					path.append(CommandType.SHOW_MAG_LIST);
				} else if (command instanceof SubscriptionRequestControllerMarker) {
					path.append(CommandType.SHOW_MAG_ITEM);
					Integer magId = Integer.parseInt(request.getParameter(FormParameter.MAG_ID));
					path.append("&").append(FormParameter.MAG_ID).append("=").append(magId);
				}
				response.sendRedirect(path.toString());
			} else {
				dispatcher.forward(request, response);
			}
		} else {

			String errorMsg = ActionHelper.findProperty(request,
					"message.nullPage");
			throw new CommandException(errorMsg);
		}
	}
}