package by.barkhan.finaltask.command;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import by.barkhan.finaltask.constant.FormParameter;
import by.barkhan.finaltask.manager.PageContentManager;
import by.barkhan.finaltask.entity.Magazine;
import by.barkhan.finaltask.logic.MagazineLogic;
import by.barkhan.finaltask.utility.PageCalculator;

public class ActionHelper {

	public static void prepareMagazineList(HttpServletRequest request,
			int offset) {
		List<Magazine> magList = MagazineLogic.findAllMagazines(offset);
		request.setAttribute(FormParameter.MAG_LIST, magList);
		int pageCount = PageCalculator.findPageCount();
		request.setAttribute(FormParameter.PAGE_COUNT, pageCount);
		request.setAttribute(FormParameter.PER_PAGE, PageCalculator.DEFAULT_PERPAGE);
	}

	public static String findProperty(HttpServletRequest request, String key) {
		PageContentManager manager = new PageContentManager();
		HttpSession session = request.getSession();
		String language = (String) session.getAttribute(FormParameter.LANGUAGE);
		if (language != null) {
			manager.changeResource(new Locale(language));
		}
		return manager.getProperty(key);
	}
}
