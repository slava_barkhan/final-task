package by.barkhan.finaltask.command;

import javax.servlet.http.HttpServletRequest;

/**
 * @author Viachaslau Barkhan
 * Base interface for all commands
 * <p> Uses pattern Command
 */
public interface ActionCommand {

	String execute(HttpServletRequest request);
}
