package by.barkhan.finaltask.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import by.barkhan.finaltask.constant.FormParameter;
import by.barkhan.finaltask.manager.ConfigManager;

public class ChangeLocaleCommand implements ActionCommand {

	@Override
	public String execute(HttpServletRequest request) {
		String page;
		String language = request.getParameter(FormParameter.LANGUAGE);
		HttpSession session = request.getSession();
		session.setAttribute(FormParameter.LANGUAGE, language);

		ActionHelper.prepareMagazineList(request, 0);
		page = ConfigManager.getProperty("page.magazine.list");

		return page;
	}
}
