package by.barkhan.finaltask.command.subscription;

import javax.servlet.http.HttpServletRequest;

import by.barkhan.finaltask.command.ActionCommand;
import by.barkhan.finaltask.entity.Subscription;
import by.barkhan.finaltask.logic.SubscriptionLogic;
import by.barkhan.finaltask.manager.ConfigManager;
import by.barkhan.finaltask.constant.FormParameter;
import by.barkhan.finaltask.entity.Magazine;

public class SelectSubscriptionCommand implements ActionCommand {

	@Override
	public String execute(HttpServletRequest request) {
		String page;
		int subId = Integer
				.parseInt(request.getParameter(FormParameter.SUB_ID));
		Subscription sub = SubscriptionLogic.findSubscriptionById(subId);
		request.setAttribute(FormParameter.SUBSCRIPTION, sub);
		Magazine mag = sub.getMagazine();
		request.setAttribute(FormParameter.MAGAZINE, mag);
		page = ConfigManager.getProperty("page.sub.update");
		return page;
	}
}
