package by.barkhan.finaltask.command.subscription;

import javax.servlet.http.HttpServletRequest;

import by.barkhan.finaltask.command.ActionCommand;
import by.barkhan.finaltask.command.marker.SubscriptionRequestControllerMarker;
import by.barkhan.finaltask.entity.Subscription;
import by.barkhan.finaltask.logic.SubscriptionLogic;
import by.barkhan.finaltask.manager.ConfigManager;
import by.barkhan.finaltask.validation.SubscriptionValidation;
import by.barkhan.finaltask.command.ActionHelper;
import by.barkhan.finaltask.constant.FormParameter;
import by.barkhan.finaltask.entity.Magazine;
import by.barkhan.finaltask.logic.MagazineLogic;

public class UpdateSubscriptionCommand implements ActionCommand, SubscriptionRequestControllerMarker {

	@Override
	public String execute(HttpServletRequest request) {
		String page;
		int magId = Integer
				.parseInt(request.getParameter(FormParameter.MAG_ID));
		int subId = Integer
				.parseInt(request.getParameter(FormParameter.SUB_ID));
		String index = request.getParameter(FormParameter.INDEX);
		String duration = request.getParameter(FormParameter.DURATION);
		String price = request.getParameter(FormParameter.PRICE);
		Subscription sub = new Subscription(subId, index, duration, price);
		Magazine mag = MagazineLogic.findMagazineById(magId);
		if (!SubscriptionValidation.checkInputParameters(request)) {
			request.setAttribute(FormParameter.SUBSCRIPTION, sub);
			request.setAttribute(FormParameter.MAGAZINE, mag);
			page = ConfigManager.getProperty("page.sub.update");
		} else {
			SubscriptionLogic.updateSubscription(sub);
			request.setAttribute(FormParameter.MAGAZINE, mag);
			String message = ActionHelper.findProperty(request,
					"success.sub.update");
			request.setAttribute(FormParameter.SUCCESS_MESSAGE, message);
			page = ConfigManager.getProperty("page.magazine.item");
		}
		return page;
	}

}
