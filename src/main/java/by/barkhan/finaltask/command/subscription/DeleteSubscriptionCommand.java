package by.barkhan.finaltask.command.subscription;

import javax.servlet.http.HttpServletRequest;

import by.barkhan.finaltask.command.ActionCommand;
import by.barkhan.finaltask.command.ActionHelper;
import by.barkhan.finaltask.constant.FormParameter;
import by.barkhan.finaltask.command.marker.SubscriptionRequestControllerMarker;
import by.barkhan.finaltask.entity.Magazine;
import by.barkhan.finaltask.logic.MagazineLogic;
import by.barkhan.finaltask.logic.SubscriptionLogic;
import by.barkhan.finaltask.manager.ConfigManager;

public class DeleteSubscriptionCommand implements ActionCommand, SubscriptionRequestControllerMarker {

	@Override
	public String execute(HttpServletRequest request) {
		String page;
		int magId = Integer
				.parseInt(request.getParameter(FormParameter.MAG_ID));
		int subId = Integer
				.parseInt(request.getParameter(FormParameter.SUB_ID));
		SubscriptionLogic.deleteSubscription(subId);
		Magazine mag = MagazineLogic.findMagazineById(magId);
		request.setAttribute(FormParameter.MAGAZINE, mag);
		String message = ActionHelper.findProperty(request,
				"success.sub.delete");
		request.setAttribute(FormParameter.SUCCESS_MESSAGE, message);
		page = ConfigManager.getProperty("page.magazine.item");
		return page;
	}
}
