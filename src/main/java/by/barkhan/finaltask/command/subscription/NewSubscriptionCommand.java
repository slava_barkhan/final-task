package by.barkhan.finaltask.command.subscription;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import by.barkhan.finaltask.manager.ConfigManager;
import by.barkhan.finaltask.command.ActionCommand;
import by.barkhan.finaltask.constant.FormParameter;
import by.barkhan.finaltask.entity.Magazine;
import by.barkhan.finaltask.logic.MagazineLogic;

public class NewSubscriptionCommand implements ActionCommand {

	@Override
	public String execute(HttpServletRequest request) {
		String page;
		List<Magazine> magList = MagazineLogic.findAllMagazines();
		request.setAttribute(FormParameter.MAG_LIST, magList);
		page = ConfigManager.getProperty("page.sub.add");
		return page;
	}
}
