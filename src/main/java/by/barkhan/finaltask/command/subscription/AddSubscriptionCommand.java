package by.barkhan.finaltask.command.subscription;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import by.barkhan.finaltask.command.ActionCommand;
import by.barkhan.finaltask.command.ActionHelper;
import by.barkhan.finaltask.constant.FormParameter;
import by.barkhan.finaltask.command.marker.SubscriptionRequestControllerMarker;
import by.barkhan.finaltask.entity.Subscription;
import by.barkhan.finaltask.logic.MagazineLogic;
import by.barkhan.finaltask.logic.SubscriptionLogic;
import by.barkhan.finaltask.manager.ConfigManager;
import by.barkhan.finaltask.validation.SubscriptionValidation;
import by.barkhan.finaltask.entity.Magazine;

public class AddSubscriptionCommand implements ActionCommand, SubscriptionRequestControllerMarker {

	@Override
	public String execute(HttpServletRequest request) {
		String page;
		int magId = Integer
				.parseInt(request.getParameter(FormParameter.MAG_ID));
		String index = request.getParameter(FormParameter.INDEX);
		String duration = request.getParameter(FormParameter.DURATION);
		String price = request.getParameter(FormParameter.PRICE);
		Subscription sub = new Subscription(index, duration, price);
		if (!SubscriptionValidation.checkInputParameters(request)) {
			request.setAttribute(FormParameter.SUBSCRIPTION, sub);
			List<Magazine> magList = MagazineLogic.findAllMagazines();
			request.setAttribute(FormParameter.MAG_LIST, magList);
			page = ConfigManager.getProperty("page.sub.add");
		} else {
			Magazine mag = MagazineLogic.findMagazineById(magId);
			sub.setMagazine(mag);
			SubscriptionLogic.createSubscription(sub);
			mag.addSubscription(sub);
			request.setAttribute(FormParameter.MAGAZINE, mag);
			String message = ActionHelper.findProperty(request,
					"success.sub.add");
			request.setAttribute(FormParameter.SUCCESS_MESSAGE, message);
			page = ConfigManager.getProperty("page.magazine.item");
		}
		return page;
	}

}
