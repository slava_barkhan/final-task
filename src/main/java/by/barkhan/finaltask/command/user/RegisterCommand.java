package by.barkhan.finaltask.command.user;

import java.text.MessageFormat;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import by.barkhan.finaltask.command.marker.UserRequestControllerMarker;
import by.barkhan.finaltask.manager.ConfigManager;
import by.barkhan.finaltask.command.ActionCommand;
import by.barkhan.finaltask.command.ActionHelper;
import by.barkhan.finaltask.constant.FormParameter;
import by.barkhan.finaltask.entity.User;
import by.barkhan.finaltask.logic.UserLogic;
import by.barkhan.finaltask.utility.Encrypter;
import by.barkhan.finaltask.validation.RegisterValidation;

public class RegisterCommand implements ActionCommand,UserRequestControllerMarker {

	@Override
	public String execute(HttpServletRequest request) {
		String page = null;
		String userName = request.getParameter(FormParameter.USERNAME);
		String login = request.getParameter(FormParameter.LOGIN);
		String pass = request.getParameter(FormParameter.PASSWORD);
		int age = Integer.parseInt(request.getParameter(FormParameter.AGE));
		User user = new User(userName, age, login);
		if (!RegisterValidation.checkInputParameters(request)) {
			request.setAttribute(FormParameter.USER, user);
			page = ConfigManager.getProperty("page.register");
		} else if (UserLogic.checkLogin(login) != null) {
			String loginError = ActionHelper.findProperty(request,
					"error.loginexist");
			request.setAttribute("loginError", loginError);
			request.setAttribute(FormParameter.USER, user);
			page = ConfigManager.getProperty("page.register");
		} else {
			String encryptPassword = Encrypter.crypt(pass);
			user.setPassword(encryptPassword);
			UserLogic.createUser(user);

			HttpSession session = request.getSession();
			session.setAttribute(FormParameter.USER, user);
			session.setAttribute(FormParameter.ROLE, user.getRoleName());
			String propertyMessage = ActionHelper.findProperty(request,
					"success.register");
			String successRegister = MessageFormat.format(propertyMessage,
					userName);
			request.setAttribute(FormParameter.SUCCESS_MESSAGE, successRegister);

			ActionHelper.prepareMagazineList(request, 0);
			page = ConfigManager.getProperty("page.start");

		}
		return page;
	}

}
