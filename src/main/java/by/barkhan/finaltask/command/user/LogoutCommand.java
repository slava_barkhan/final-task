package by.barkhan.finaltask.command.user;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import by.barkhan.finaltask.manager.ConfigManager;
import by.barkhan.finaltask.command.ActionCommand;

public class LogoutCommand implements ActionCommand {
	@Override
	public String execute(HttpServletRequest request) {
		String page = null;
		HttpSession session = request.getSession(false);
		if (session != null) {
			session.invalidate();
		}
		page = ConfigManager.getProperty("page.register");
		return page;
	}
}