package by.barkhan.finaltask.command.reservation;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import by.barkhan.finaltask.command.ActionCommand;
import by.barkhan.finaltask.command.marker.ReservationRequestControllerMarker;
import by.barkhan.finaltask.entity.Reservation;
import by.barkhan.finaltask.entity.User;
import by.barkhan.finaltask.logic.ReservationLogic;
import by.barkhan.finaltask.manager.ConfigManager;
import by.barkhan.finaltask.constant.FormParameter;

public class DeleteReservationCommand implements ActionCommand, ReservationRequestControllerMarker {

	@Override
	public String execute(HttpServletRequest request) {
		String page = ConfigManager.getProperty("page.index");
		int resId = Integer
				.parseInt(request.getParameter(FormParameter.RES_ID));
		ReservationLogic.deleteReservation(resId);
		HttpSession session = request.getSession(false);
		if (session != null) {
			User user = (User) (session.getAttribute(FormParameter.USER));
			String roleName = user.getRoleName();
			if ("reader".equals(roleName)) {
				int userId = user.getId();
				List<Reservation> resList = ReservationLogic
						.findByUserId(userId);
				request.setAttribute(FormParameter.RES_LIST, resList);
				page = ConfigManager.getProperty("page.res.item");
			} else if ("moderator".equals(roleName)) {
				List<Reservation> resList = ReservationLogic
						.findAllreservations();
				request.setAttribute(FormParameter.RES_LIST, resList);
				page = ConfigManager.getProperty("page.res.list");
			}
		}
		return page;
	}
}
