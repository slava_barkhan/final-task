package by.barkhan.finaltask.command.reservation;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import by.barkhan.finaltask.command.marker.ReservationRequestControllerMarker;
import by.barkhan.finaltask.logic.ReservationLogic;
import by.barkhan.finaltask.manager.ConfigManager;
import by.barkhan.finaltask.command.ActionCommand;
import by.barkhan.finaltask.constant.FormParameter;
import by.barkhan.finaltask.entity.Reservation;

public class UpdateReservationCommand implements ActionCommand, ReservationRequestControllerMarker {

	@Override
	public String execute(HttpServletRequest request) {
		String page;
		int resId = Integer
				.parseInt(request.getParameter(FormParameter.RES_ID));
		int statusId = Integer.parseInt(request
				.getParameter(FormParameter.STATUS_ID));
		ReservationLogic.changeStatus(resId, statusId);
		List<Reservation> resList = ReservationLogic.findAllreservations();
		request.setAttribute(FormParameter.RES_LIST, resList);
		page = ConfigManager.getProperty("page.res.list");
		return page;
	}

}
