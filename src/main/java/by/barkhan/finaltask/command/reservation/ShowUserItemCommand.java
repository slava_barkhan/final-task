package by.barkhan.finaltask.command.reservation;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import by.barkhan.finaltask.logic.ReservationLogic;
import by.barkhan.finaltask.manager.ConfigManager;
import by.barkhan.finaltask.command.ActionCommand;
import by.barkhan.finaltask.constant.FormParameter;
import by.barkhan.finaltask.entity.Reservation;
import by.barkhan.finaltask.entity.User;

public class ShowUserItemCommand implements ActionCommand {

	@Override
	public String execute(HttpServletRequest request) {
		String page = ConfigManager.getProperty("page.index");
		HttpSession session = request.getSession();
		if (session.getAttribute(FormParameter.USER) != null) {
			User user = (User) session.getAttribute(FormParameter.USER);
			int id = user.getId();
			List<Reservation> resList = ReservationLogic.findByUserId(id);
			request.setAttribute(FormParameter.RES_LIST, resList);
			page = ConfigManager.getProperty("page.res.item");
		}
		return page;
	}

}
