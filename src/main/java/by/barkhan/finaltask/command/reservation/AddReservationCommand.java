package by.barkhan.finaltask.command.reservation;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import by.barkhan.finaltask.command.marker.ReservationRequestControllerMarker;
import by.barkhan.finaltask.logic.ReservationLogic;
import by.barkhan.finaltask.manager.ConfigManager;
import by.barkhan.finaltask.command.ActionCommand;
import by.barkhan.finaltask.constant.FormParameter;
import by.barkhan.finaltask.entity.Reservation;
import by.barkhan.finaltask.entity.User;

public class AddReservationCommand implements ActionCommand, ReservationRequestControllerMarker {

	@Override
	public String execute(HttpServletRequest request) {
		String page = ConfigManager.getProperty("page.index");
		HttpSession session = request.getSession(false);
		if (session != null) {
			int userId = ((User) session.getAttribute(FormParameter.USER))
					.getId();
			int subId = Integer.parseInt(request.getParameter(FormParameter.SUB_ID));
			int count = Integer.parseInt(request.getParameter(FormParameter.COUNT));
			ReservationLogic.createReservation(userId, subId, count);
			List<Reservation> resList = ReservationLogic.findByUserId(userId);
			request.setAttribute(FormParameter.RES_LIST, resList);
			page = ConfigManager.getProperty("page.res.item");
		}
		return page;
	}

}
