package by.barkhan.finaltask.command;

import javax.servlet.http.HttpServletRequest;

import by.barkhan.finaltask.manager.ConfigManager;

public class EmptyCommand implements ActionCommand {
	@Override
	public String execute(HttpServletRequest request) {
		String page = ConfigManager.getProperty("page.index");
		return page;
	}
}
