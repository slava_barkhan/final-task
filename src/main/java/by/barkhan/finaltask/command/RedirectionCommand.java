package by.barkhan.finaltask.command;

import javax.servlet.http.HttpServletRequest;

public class RedirectionCommand implements ActionCommand {

	@Override
	public String execute(HttpServletRequest request) {
		String page = request.getParameter("page");
		return page;
	}
}
