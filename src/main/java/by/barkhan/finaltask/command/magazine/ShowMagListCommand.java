package by.barkhan.finaltask.command.magazine;

import javax.servlet.http.HttpServletRequest;

import by.barkhan.finaltask.command.ActionCommand;
import by.barkhan.finaltask.command.ActionHelper;
import by.barkhan.finaltask.constant.FormParameter;
import by.barkhan.finaltask.manager.ConfigManager;

public class ShowMagListCommand implements ActionCommand {
	private final int DEFAULT_PERPAGE = 9;

	@Override
	public String execute(HttpServletRequest request) {
		String page;
		String sequence = request.getParameter(FormParameter.SEQUENCE);
		int offset = 0;
		if (sequence != null) {
			offset = Integer.parseInt(sequence) * DEFAULT_PERPAGE;
			request.setAttribute(FormParameter.SEQUENCE, sequence);
		}
		ActionHelper.prepareMagazineList(request, offset);
		page = ConfigManager.getProperty("page.magazine.list");
		return page;
	}

}
