package by.barkhan.finaltask.command.magazine;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import by.barkhan.finaltask.command.ActionCommand;
import by.barkhan.finaltask.constant.FormParameter;
import by.barkhan.finaltask.entity.Period;
import by.barkhan.finaltask.logic.MagazineLogic;
import by.barkhan.finaltask.manager.ConfigManager;
import by.barkhan.finaltask.entity.Magazine;

public class SelectMagazineCommand implements ActionCommand {

	@Override
	public String execute(HttpServletRequest request) {
		String page;
		int id = Integer.parseInt(request.getParameter(FormParameter.MAG_ID));
		Magazine mag = MagazineLogic.findMagazineById(id);
		request.setAttribute(FormParameter.MAGAZINE, mag);
		List<Period> periodList = MagazineLogic.findAllPeriods();
		request.setAttribute(FormParameter.PERIOD_LIST, periodList);
		page = ConfigManager.getProperty("page.magazine.update");
		return page;
	}

}
