package by.barkhan.finaltask.command.magazine;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import by.barkhan.finaltask.command.ActionCommand;
import by.barkhan.finaltask.entity.Period;
import by.barkhan.finaltask.manager.ConfigManager;
import by.barkhan.finaltask.constant.FormParameter;
import by.barkhan.finaltask.logic.MagazineLogic;

public class NewMagazineCommand implements ActionCommand {

	@Override
	public String execute(HttpServletRequest request) {
		String page;
		List<Period> periodList = MagazineLogic.findAllPeriods();
		request.setAttribute(FormParameter.PERIOD_LIST, periodList);
		page = ConfigManager.getProperty("page.magazine.add");
		return page;
	}

}
