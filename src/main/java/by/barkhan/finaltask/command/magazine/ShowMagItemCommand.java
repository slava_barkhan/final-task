package by.barkhan.finaltask.command.magazine;

import javax.servlet.http.HttpServletRequest;

import by.barkhan.finaltask.manager.ConfigManager;
import by.barkhan.finaltask.command.ActionCommand;
import by.barkhan.finaltask.constant.FormParameter;
import by.barkhan.finaltask.entity.Magazine;
import by.barkhan.finaltask.logic.MagazineLogic;

public class ShowMagItemCommand implements ActionCommand {

	@Override
	public String execute(HttpServletRequest request) {
		String page;
		int id = Integer.parseInt(request.getParameter(FormParameter.MAG_ID));
		Magazine mag = MagazineLogic.findMagazineById(id);
		request.setAttribute(FormParameter.MAGAZINE, mag);
		page = ConfigManager.getProperty("page.magazine.item");
		return page;
	}

}
