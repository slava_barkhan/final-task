package by.barkhan.finaltask.command.magazine;

import javax.servlet.http.HttpServletRequest;

import by.barkhan.finaltask.command.ActionCommand;
import by.barkhan.finaltask.command.marker.MagazineRequestControllerMarker;
import by.barkhan.finaltask.manager.ConfigManager;
import by.barkhan.finaltask.command.ActionHelper;
import by.barkhan.finaltask.constant.FormParameter;
import by.barkhan.finaltask.logic.MagazineLogic;

public class DeleteMagazineCommand implements ActionCommand, MagazineRequestControllerMarker {

	@Override
	public String execute(HttpServletRequest request) {
		String page;

		int id = Integer.parseInt(request.getParameter(FormParameter.MAG_ID));
		MagazineLogic.deleteMagazine(id);
		ActionHelper.prepareMagazineList(request, 0);
		String msg = ActionHelper.findProperty(request, "success.magazine.delete");
		request.setAttribute(FormParameter.SUCCESS_MESSAGE, msg);

		page = ConfigManager.getProperty("page.magazine.list");
		return page;
	}

}
