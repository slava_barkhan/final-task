package by.barkhan.finaltask.command.magazine;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import by.barkhan.finaltask.command.ActionCommand;
import by.barkhan.finaltask.command.ActionHelper;
import by.barkhan.finaltask.constant.FormParameter;
import by.barkhan.finaltask.command.marker.MagazineRequestControllerMarker;
import by.barkhan.finaltask.entity.Period;
import by.barkhan.finaltask.manager.ConfigManager;
import by.barkhan.finaltask.entity.Magazine;
import by.barkhan.finaltask.logic.MagazineLogic;
import by.barkhan.finaltask.validation.MagazineValidation;

public class UpdateMagazineCommand implements ActionCommand, MagazineRequestControllerMarker {

	@Override
	public String execute(HttpServletRequest request) {
		String page;
		int id = Integer.parseInt(request.getParameter(FormParameter.MAG_ID));
		String title = request.getParameter(FormParameter.TITLE);
		String annotation = request.getParameter(FormParameter.ANNOTATION);
		int periodId = Integer.parseInt(request
				.getParameter(FormParameter.PERIOD_ID));
		Magazine mag = new Magazine(id, title, annotation);
		if (!MagazineValidation.checkInputParameters(request)) {
			request.setAttribute(FormParameter.MAGAZINE, mag);
			request.setAttribute(FormParameter.PERIOD_ID, periodId);
			List<Period> periodList = MagazineLogic.findAllPeriods();
			request.setAttribute(FormParameter.PERIOD_LIST, periodList);
			page = ConfigManager.getProperty("page.magazine.update");
		} else {
			MagazineLogic.updateMagazine(periodId, mag);
			ActionHelper.prepareMagazineList(request, 0);
			String msg = ActionHelper.findProperty(request,
					"success.magazine.update");
			request.setAttribute(FormParameter.SUCCESS_MESSAGE, msg);
			page = ConfigManager.getProperty("page.magazine.list");

		}
		return page;
	}

}
