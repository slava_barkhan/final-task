package by.barkhan.finaltask.command.magazine;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import by.barkhan.finaltask.command.ActionCommand;
import by.barkhan.finaltask.command.ActionHelper;
import by.barkhan.finaltask.constant.FormParameter;
import by.barkhan.finaltask.command.marker.MagazineRequestControllerMarker;
import by.barkhan.finaltask.entity.Period;
import by.barkhan.finaltask.logic.MagazineLogic;
import by.barkhan.finaltask.manager.ConfigManager;
import by.barkhan.finaltask.entity.Magazine;
import by.barkhan.finaltask.validation.MagazineValidation;

public class AddMagazineCommand implements ActionCommand, MagazineRequestControllerMarker {

	@Override
	public String execute(HttpServletRequest request) {
		String page;
		String title = request.getParameter(FormParameter.TITLE);
		String annotation = request.getParameter(FormParameter.ANNOTATION);
		int periodId = Integer.parseInt(request
				.getParameter(FormParameter.PERIOD_ID));
		String location = request.getParameter(FormParameter.LOCATION);
		Magazine mag = new Magazine(title, annotation);
		mag.setLocation(location);
		if (!MagazineValidation.checkInputParameters(request)) {
			request.setAttribute(FormParameter.MAGAZINE, mag);
			request.setAttribute(FormParameter.PERIOD_ID, periodId);
			List<Period> periodList = MagazineLogic.findAllPeriods();
			request.setAttribute(FormParameter.PERIOD_LIST, periodList);
			page = ConfigManager.getProperty("page.magazine.add");
		} else {
			MagazineLogic.createMagazine(periodId, mag);
			ActionHelper.prepareMagazineList(request, 0);
			String msg = ActionHelper.findProperty(request, "success.magazine.add");
			request.setAttribute(FormParameter.SUCCESS_MESSAGE, msg);
			page = ConfigManager.getProperty("page.magazine.list");

		}
		return page;
	}
}
