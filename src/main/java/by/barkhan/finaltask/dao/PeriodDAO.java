package by.barkhan.finaltask.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import by.barkhan.finaltask.constant.QuerySQL;
import by.barkhan.finaltask.dbhelper.ProxyConnection;
import by.barkhan.finaltask.dbhelper.ResultSetCreator;
import by.barkhan.finaltask.entity.Period;
import by.barkhan.finaltask.exception.DAOException;

public class PeriodDAO extends AbstractDAO<Integer, Period> {

	public PeriodDAO(ProxyConnection connection) {
		super(connection);
	}

	@Override
	public List<Period> findAll() throws DAOException {
		List<Period> periodList = new ArrayList<>();
		try (Statement statement = connection.createStatement()) {
			ResultSet rs = statement.executeQuery(QuerySQL.FIND_ALL_PERIODS);
			while (rs.next()) {
				Period period = ResultSetCreator.createPeriod(rs);
				periodList.add(period);
			}
			return periodList;
		} catch (SQLException e) {
			throw new DAOException(e);
		}
	}

	@Override
	public Period findEntityById(Integer id) throws DAOException {
		try (PreparedStatement statement = connection
				.prepareStatement(QuerySQL.FIND_PERIOD_BY_ID)) {
			statement.setInt(1, id);
			ResultSet rs = statement.executeQuery();
			if (rs.next()) {
				Period period = ResultSetCreator.createPeriod(rs);
				return period;
			}
			
		} catch (SQLException e) {
			throw new DAOException(e);
		}
		throw new DAOException("Can't calculate period");
	}

	@Override
	public void delete(Integer id) throws DAOException {
		throw new DAOException("Can't delete period");
	}

	@Override
	public Integer create(Period entity) throws DAOException {
		throw new DAOException("Can't create new period");
	}

	@Override
	public void update(Period entity) throws DAOException {
		throw new DAOException("Can't update period");
	}

}
