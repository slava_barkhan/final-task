package by.barkhan.finaltask.validation;

import javax.servlet.http.HttpServletRequest;

import by.barkhan.finaltask.utility.Checker;
import by.barkhan.finaltask.command.ActionHelper;
import by.barkhan.finaltask.constant.FormParameter;

public class RegisterValidation {
	private final static int PASS_MIN_LENGTH = 6;
	private final static int PASS_MAX_LENGTH = 16;
	private final static int AGE_MIN = 18;
	private final static int AGE_MAX = 100;

	public static boolean checkInputParameters(HttpServletRequest request) {
		boolean flag = true;
		String userName = request.getParameter(FormParameter.USERNAME);
		String login = request.getParameter(FormParameter.LOGIN);
		String pass = request.getParameter(FormParameter.PASSWORD);
		String passConfirm = request.getParameter(FormParameter.PASS_CONFIRM);
		String age = request.getParameter(FormParameter.AGE);
		String emptyField = ActionHelper.findProperty(request, "error.empty");
		if (checkEmpty(userName)) {
			request.setAttribute(FormParameter.USERNAME_ERROR, emptyField);
			flag = false;
		}
		if (checkEmpty(login)) {
			request.setAttribute(FormParameter.LOGIN_ERROR, emptyField);
			flag = false;
		}
		if (!checkPassword(pass)) {
			String passwordError = ActionHelper.findProperty(request,
					"error.password.input");
			request.setAttribute(FormParameter.PASSWORD_ERROR, passwordError);
			flag = false;
		} else if (!checkConfirmPassword(pass, passConfirm)) {
			String passwordError = ActionHelper.findProperty(request,
					"error.password.confirm");
			request.setAttribute(FormParameter.PASS_CONFIRM_ERROR, passwordError);
			flag = false;
		}
		if (!checkAge(age)) {
			String ageError = ActionHelper.findProperty(request, "error.age");
			request.setAttribute(FormParameter.AGE_ERROR, ageError);
			flag = false;
		}
		return flag;
	}

	private static boolean checkPassword(String enterPassword) {
		boolean flag = true;
		if (checkEmpty(enterPassword)) {
			flag = false;
		}
		int length = enterPassword.length();
		if (length < PASS_MIN_LENGTH || length > PASS_MAX_LENGTH) {
			flag = false;
		}
		return flag;
	}

	private static boolean checkConfirmPassword(String pass, String confirmPass) {
		boolean flag = true;
		if (checkEmpty(confirmPass)) {
			flag = false;
		}
		if (!pass.equals(confirmPass)) {
			flag = false;
		}
		return flag;
	}

	private static boolean checkAge(String age) {
		boolean flag = true;
		if (!Checker.isInteger(age)) {
			flag = false;
		}
		int realAge = Integer.parseInt(age);
		if (realAge < AGE_MIN || realAge > AGE_MAX) {
			flag = false;
		}
		return flag;
	}

	private static boolean checkEmpty(String field) {
		return field == null || "".equals(field.trim());
	}
}
