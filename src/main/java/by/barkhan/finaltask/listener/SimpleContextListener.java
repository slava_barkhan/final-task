package by.barkhan.finaltask.listener;

import java.io.File;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import by.barkhan.finaltask.dbhelper.ConnectionPool;

@WebListener
public class SimpleContextListener implements ServletContextListener {

	@Override
	public void contextDestroyed(ServletContextEvent e) {
		ConnectionPool.getInstance().closePool();
	}

	@Override
	public void contextInitialized(ServletContextEvent e) {
		ServletContext ctx = e.getServletContext();

		try {
			DriverManager.registerDriver(new com.mysql.jdbc.Driver());
		} catch (SQLException exc) {
			ctx.log("Can't register mysql driver!", exc);
		}
	}

}
