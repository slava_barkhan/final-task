package by.barkhan.finaltask.listener;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@WebListener
public class SimpleSessionListener implements HttpSessionListener {
	private static final Logger LOG = LogManager.getLogger(SimpleSessionListener.class);

	@Override
	public void sessionCreated(HttpSessionEvent arg0) {
		LOG.log(Level.DEBUG,"session created");
	}

	@Override
	public void sessionDestroyed(HttpSessionEvent arg0) {
		LOG.log(Level.DEBUG,"session destroyed");
	}

}
