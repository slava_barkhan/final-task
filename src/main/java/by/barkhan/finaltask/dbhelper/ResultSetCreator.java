package by.barkhan.finaltask.dbhelper;

import java.sql.ResultSet;
import java.sql.SQLException;

import by.barkhan.finaltask.entity.Period;
import by.barkhan.finaltask.entity.Reservation;
import by.barkhan.finaltask.entity.Subscription;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.barkhan.finaltask.entity.Magazine;
import by.barkhan.finaltask.entity.Status;
import by.barkhan.finaltask.entity.User;

public class ResultSetCreator {
	private static final Logger LOG = LogManager.getLogger(ResultSetCreator.class);

	public static Subscription createSubscription(ResultSet rs) {
		Subscription sub = null;
		try {
			int id = rs.getInt("subscription_id");
			String subIndex = rs.getString("sub_index");
			String duration = rs.getString("duration");
			String price = rs.getString("price");
			sub = new Subscription(id, subIndex, duration, price);
			Magazine mag = createMagazine(rs);
			sub.setMagazine(mag);
		} catch (SQLException e) {
			LOG.log(Level.ERROR,e);
		}
		return sub;
	}

	public static Magazine createMagazine(ResultSet rs) {
		Magazine magazine = null;
		try {
			int id = rs.getInt("magazine_id");
			String title = rs.getString("title");
			String annotation = rs.getString("annotation");
			String location = rs.getString("location");
			magazine = new Magazine(id, title, annotation);
			if (location != null && !"".equals(location.trim())) {
				magazine.setLocation(location);
			}
			Period period = createPeriod(rs);
			magazine.setPeriod(period);
		} catch (SQLException e) {
			LOG.log(Level.ERROR,e);
		}
		return magazine;
	}

	public static Period createPeriod(ResultSet rs) {
		Period period = null;
		try {
			int id = rs.getInt("period_id");
			String periodicity = rs.getString("periodicity");
			period = new Period(id, periodicity);
		} catch (SQLException e) {
			LOG.log(Level.ERROR,e);
		}
		return period;
	}

	public static User createUser(ResultSet rs) {
		User user = null;
		try {
			int id = rs.getInt("user_id");
			String userName = rs.getString("user_name");
			int age = rs.getInt("age");
			String login = rs.getString("login");
			user = new User(id, userName, age, login);
			String rolename = rs.getString("role_name");
			user.setRoleName(rolename);
		} catch (SQLException e) {
			LOG.log(Level.ERROR,e);
		}
		return user;
	}

	public static Reservation createReservation(ResultSet rs) {
		Reservation reserv = null;
		try {
			int id = rs.getInt("id");
			int count = rs.getInt("count");
			reserv = new Reservation(id, count);
		} catch (SQLException e) {
			LOG.log(Level.ERROR,e);
		}
		User user = ResultSetCreator.createUser(rs);
		Subscription sub = ResultSetCreator.createSubscription(rs);
		Status status = ResultSetCreator.createStatus(rs);
		reserv.setUser(user);
		reserv.setSubscription(sub);
		reserv.setStatus(status);
		return reserv;
	}

	public static Status createStatus(ResultSet rs) {
		Status status = null;
		try {
			int id = rs.getInt("status_id");
			String description = rs.getString("status_description");
			status = new Status(id, description);
		} catch (SQLException e) {
			LOG.log(Level.ERROR,e);
		}
		return status;
	}
}
