package by.barkhan.finaltask.dbhelper;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.barkhan.finaltask.exception.DAOException;
import by.barkhan.finaltask.manager.DatabaseManager;

public class ConnectionPool {
	private static final Logger LOG = LogManager.getLogger(ConnectionPool.class);
	private AtomicBoolean flag = new AtomicBoolean(true);
	private ArrayBlockingQueue<ProxyConnection> connectionQueue;
	private static ConnectionPool instance = null;
	private static AtomicBoolean instanceCreated = new AtomicBoolean(false);
	private static Lock lock = new ReentrantLock();

	private Connection initConnection() {
		String url = DatabaseManager.getProperty("db.url");
		String user = DatabaseManager.getProperty("db.user");
		String password = DatabaseManager.getProperty("db.password");
		Connection con;
		try {
			con = DriverManager.getConnection(url, user, password);
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return con;
	}

	private ConnectionPool() {
		int size = Integer.parseInt(DatabaseManager.getProperty("db.poolsize"));
		connectionQueue = new ArrayBlockingQueue<>(size);
		for (int i = 0; i < size; i++) {
			Connection conn = initConnection();
			ProxyConnection connection = new ProxyConnection(conn);
			connectionQueue.offer(connection);
		}
	}

	public static ConnectionPool getInstance() {
		if (!instanceCreated.get()) {
			try {
				lock.lock();
				if (!instanceCreated.get()) {
					instance = new ConnectionPool();
					instanceCreated.set(true);
				}
			} finally {
				lock.unlock();
			}
		}
		return instance;
	}

	public ProxyConnection getConnection() throws DAOException {
		if (flag.get()) {
			try {
				ProxyConnection connection = connectionQueue.poll(5, TimeUnit.SECONDS);
				return connection;
			} catch (InterruptedException e) {
				LOG.log(Level.ERROR,e);
			}
		}
		throw new DAOException("Can't get connection");
	}

	public void closeConnection(ProxyConnection connection) {
		if (connection != null) {
			connectionQueue.offer(connection);
		}
	}

	public void closePool() {
		flag.set(false);
		try {
			TimeUnit.SECONDS.sleep(10);
		} catch (InterruptedException exc) {
			LOG.log(Level.ERROR,exc);
		}
		for (ProxyConnection con : connectionQueue) {
			close(con);
		}
	}

	private void close(Connection con) {
		try {
			if (con != null) {
				con.close();
			}
		} catch (SQLException e) {
			LOG.log(Level.ERROR,e);
		}

	}
}
