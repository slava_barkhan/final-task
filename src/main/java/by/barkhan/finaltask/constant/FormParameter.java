package by.barkhan.finaltask.constant;

public class FormParameter {
	// id
	public static final String USER_ID = "user_id";
	public static final String MAG_ID = "mag_id";
	public static final String SUB_ID = "sub_id";
	public static final String RES_ID = "res_id";
	public static final String STATUS_ID = "status_id";

	// user params
	public static final String USER = "user";
	public static final String ROLE = "role";
	public static final String USERNAME = "userName";
	public static final String LOGIN = "login";
	public static final String PASSWORD = "password";
	public static final String PASS_CONFIRM = "passwordConfirm";
	public static final String AGE = "age";

	// magazine params
	public static final String MAGAZINE = "mag";
	public static final String MAG_LIST = "magList";
	public static final String TITLE = "title";
	public static final String ANNOTATION = "annotation";
	public static final  String PERIOD_ID = "period_id";
	public static final String LOCATION = "location";

	// subscription params
	public static final String SUBSCRIPTION = "sub";
	public static final String SUB_LIST = "subList";
	public static final String INDEX = "index";
	public static final String DURATION = "duration";
	public static final String PRICE = "price";

	// errors
	public static final String LOGIN_ERROR = "loginError";
	public static final String USERNAME_ERROR = "userNameError";
	public static final String PASSWORD_ERROR = "passwordError";
	public static final String PASS_CONFIRM_ERROR = "passwordConfirmError";
	public static final String AGE_ERROR = "ageError";
	public static final String TITLE_ERROR = "titleError";
	public static final String ANNOTATION_ERROR = "annotationError";
	public static final String PERIODICITY_ERROR = "periodicityError";
	public static final String PRICE_ERROR = "priceError";
	public static final String DURATION_ERROR = "durationError";
	public static final String INDEX_ERROR = "indexError";

	// messages
	public static final String SUCCESS_MESSAGE = "successMessage";

	// pagination params
	public static final String SEQUENCE = "sequence"; // page number
	public static final String PAGE_COUNT = "pageCount";
	public static final String PER_PAGE = "perPage"; // default amount per page

	// locale
	public static final String LANGUAGE = "language";

	public static final String COUNT = "count";

	public static final String RES_LIST = "resList";
	public static final String PERIOD_LIST = "periodList";
	public static final String COMMAND = "command";
}
