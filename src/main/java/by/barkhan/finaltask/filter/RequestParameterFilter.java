package by.barkhan.finaltask.filter;

import java.io.IOException;

import javax.servlet.DispatcherType;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.barkhan.finaltask.constant.FormParameter;
import by.barkhan.finaltask.utility.Checker;
import by.barkhan.finaltask.logic.MagazineLogic;
import by.barkhan.finaltask.utility.PageCalculator;

@WebFilter(dispatcherTypes = { DispatcherType.REQUEST, DispatcherType.FORWARD,
		DispatcherType.INCLUDE }, urlPatterns = { "/controller/*" }, initParams = { @WebInitParam(name = "INDEX_PATH", value = "/index.jsp") })
public class RequestParameterFilter implements Filter {

	private String indexPath;

	public void init(FilterConfig fConfig) throws ServletException {
		indexPath = fConfig.getInitParameter("INDEX_PATH");
	}

	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		HttpServletResponse httpResponse = (HttpServletResponse) response;

		boolean flag = true;

		if (!checkSequence(httpRequest) || !checkMagazineId(httpRequest)) {
			flag = false;
		}

		if (flag) {
			chain.doFilter(request, response);
		} else {
			forward(httpRequest, httpResponse);
		}
	}

	private boolean checkMagazineId(HttpServletRequest httpRequest) {
		boolean flag = true;
		String magId = httpRequest.getParameter(FormParameter.MAG_ID);
		if (magId != null) {
			if (Checker.isInteger(magId)) {
				int magazineId = Integer.parseInt(magId);
				if (MagazineLogic.findMagazineById(magazineId) == null) {
					flag = false;
				}
			} else {
				flag = false;
			}
		}
		return flag;
	}

	private boolean checkSequence(HttpServletRequest httpRequest) {
		boolean flag = true;
		String sequence = httpRequest.getParameter(FormParameter.SEQUENCE);
		Integer pageCount = PageCalculator.findPageCount();
		if (sequence != null) {
			if (Checker.isInteger(sequence)) {
				int sequenceNum = Integer.parseInt(sequence);

				if (sequenceNum >= pageCount || sequenceNum < 0) {
					flag = false;
				}
			} else {
				flag = false;
			}
		}
		return flag;
	}

	private void forward(HttpServletRequest httpRequest,
			HttpServletResponse httpResponse) throws ServletException,
			IOException {
		RequestDispatcher dispatcher = httpRequest.getServletContext()
				.getRequestDispatcher(indexPath);
		dispatcher.forward(httpRequest, httpResponse);
	}

	public void destroy() {
	}

}
